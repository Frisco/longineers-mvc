<?php

require_once '../src/Model.php';
class ModelTest extends PHPUnit_Framework_TestCase {
  public function testHasDataProperties() {
    $oModel = new Model();
    
    $this->assertClassHasAttribute( 'sString', 'Model' );
  }
  
  public function testNewInstanceContainsInitialValue() {
    $oModel = new Model();
    $this->assertAttributeEmpty ( 'sString', $oModel, 'Attribute not empty when no initial value supplied.' );

    $sExpected = 'somethingtostartwith';
    $oModel    = new Model( $sExpected );
    $this->assertAttributeEquals( $sExpected, 'sString', $oModel );
    
  }
}

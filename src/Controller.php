<?php

class Controller {
  protected $oModel;

  public function __construct( $p_oModel ) {
    $this->oModel = $p_oModel;
  }
  
  public function click() {
    $this->oModel->setString( 'Updated Data.' );
  }
}
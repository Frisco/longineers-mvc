<?php

class View {
  protected $oModel;
    
  public function __construct( $p_oModel ) {
    $this->oModel      = $p_oModel;
  }
  
  public function show() {
    return <<<EOF
    <DIV>
      <A href='index.php?action=click'>{$this->oModel->getString()}</A> 
    </DIV>
EOF;
  }
}
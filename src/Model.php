<?php

class Model {
  protected $sString;
  
  public function __construct( $sString = null ) {
    $this->sString = $sString;
  }
  
  public function getString() {
    return $this->sString;
  }
  
  public function setString( $p_sString ) {
    $this->sString = $p_sString;
  }
}